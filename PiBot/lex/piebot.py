from __future__ import print_function
import boto3
import os
import time
import datetime
import logging
import json
import pi_mock_api

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

orgId = 'AAA'
empId = '7519'
empName = ''


# --- Helpers that build all of the responses ---

# generate confirm intent response tells lex that the user is comforming the input.
def confirm_intent(session_attributes, intent_name, slots,message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intent_name,
            'slots': slots,
            'message': message
        }
    }

# generate close response that tells lex to close the session.
def close(session_attributes, fulfillment_state, message):
    response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message
        }
    }

    return response




# --- helper functions ---
def is_number(s):
	try:
		float(s)
		return True
	except ValueError:
		return False
		
# get rid of the unit and return only the numeric part
def get_num(n):
	n = str(n).replace('USD','')
	return n

# filter the api returned result for display
def filter_info(info):
	filtered = {}
	filtered['period_gross_pay'] = info['grossPayPeriodStr']
	filtered['period_net_pay'] = info['netPayStr']
	filtered['working_hours'] = info['hours']
	filtered['employer_contributions'] = info['employerContributionsStr']
	filtered['period_statutory_deductions'] = info['statutoryDeductionsPeriodStr']
	filtered['period_start_date'] = info['periodStartDate']
	filtered['period_end_date'] = info['periodEndDate']
	filtered['pay_date'] = info['payDate']
	return filtered

# format the payroll information for display
def format_info(info):
	result = ''
	for key in info:
		result = result +'`'+key+':'+'`'+'  '*(30-len(str(key)))+str(info[key])+'\n'
	return result

# check if the criteria id is in the session attribute, if not then try retrieve id or create new id
def get_criteria(sessionAttributes):
    if sessionAttributes and sessionAttributes.get('CriteriaId',None):
        return sessionAttributes['CriteriaId']
    crt = pi_mock_api.get_criteria(orgId,empId)
    if not crt:
        return pi_mock_api.create_criteria(orgId,empId)
    return crt

# deprecated
def generate_comparison_1(c_info, p_info):
	title = ['current period', 'previous period', 'difference']
	keys = c_info.keys()
	c_value = c_info.values()
	p_value = p_info.values()
	row_format ="{:<10}" * (len(title) + 1)
	result = row_format.format("     ",*title)+'\n'
	for k,cv,pv in zip(keys,c_value,p_value):
		if is_number(cv) and float(cv) != 0.0:
			row = [cv,pv,cv-pv]
		else:
			row = [cv,pv,0]
		result = result + row_format.format(k+'   ',*row) + '\n'
	return result

# generate and format the comparison
def generate_comparison(c_info, p_info):
	result = '\n*attributes*'+' '*41+'*previous period*'+' '*25+'*currentperiod*'+' '*18+'*difference*\n'
	keys = c_info.keys()
	c_value = c_info.values()
	p_value = p_info.values()
	for k,cv,pv in zip(keys,c_value,p_value):
		c = get_num(cv)
		p = get_num(pv)
		if is_number(c) and float(c) != 0.0:
			result = result+'`'+k+'`'+'  '*(31-len(k))+str(pv)+'  '*(21-len(str(pv)))+str(cv)+'  '*(24-len(str(cv)))+str(float(c)-float(p))+'\n'
		else:
			result = result+'`'+k+'`'+'  '*(31-len(k))+str(pv)+'  '*(21-len(str(pv)))+str(cv)+'  '*(24-len(str(cv)))+'/'+'\n'
	return result

# format the payroll information detial, e.g. deduction
def format_detail(detail):
	detail = detail['payClassificationTypes']
	result = ''
	for item in detail:
		logger.debug(item)
		result = result +'`'+str(item['label'])+':'+'`'+'  '*(30-len(str(item['label'])))+str(item['periodToDate']['amount'])+str(item['periodToDate']['currencyCode'])+'\n'
	return result

# --- functions to handle intent requests ---

# get employee information and send back a close response to lex
def emp_info(intent_request):
	slots = intent_request['currentIntent']['slots']
	startDate = slots['StartDate']
	endDate = slots['EndDate']

	logger.debug('getting payroll result from {} to {}'.format(startDate,endDate))
	sessionAttributes = intent_request.get('sessionAttributes',{})
	searchCriteria = sessionAttributes['CriteriaId']
	resultId = sessionAttributes['CurrentResultId']
	info = pi_mock_api.get_payroll_result(orgId,resultId)
	sessionAttributes['StartDate'] = info['periodStartDate']
	sessionAttributes['EndDate'] = info['periodEndDate']
	sessionAttributes['PayrollRunId'] = info['payrollRunId']
	sessionAttributes['CurPeriodInfo'] = json.dumps(info)
	logger.debug('CurrentInfo: {}'.format(json.dumps(info)))
	info = format_info(filter_info(info))
	return close(
		sessionAttributes,
		'Fulfilled',
		{
			'contentType':'PlainText',
			'content':info
		}
	)

# get payroll comparison information and send back close response
def payroll_comparison(intent_request):
	sessionAttributes = intent_request.get('sessionAttributes',{})
	searchCriteria = sessionAttributes['CriteriaId']

	currentInfo = filter_info(json.loads(sessionAttributes['CurPeriodInfo']))
	preId = sessionAttributes['PreviousResultId']
	previousInfo = filter_info(pi_mock_api.get_payroll_result(orgId,preId))
	result = generate_comparison(currentInfo,previousInfo)

	return close(
        sessionAttributes,
        'Fulfilled',
        {
            'contentType':'PlainText',
            'content':result
        }
    )
	
# generate issue that needed to be reported
def report_error(intent_request):
	slots = intent_request['currentIntent']['slots']
	reportContent = slots['ReportContent']
	sessionAttributes = intent_request.get('sessionAttributes',{})
	searchCriteria = sessionAttributes['CriteriaId']
	payrollRunId = sessionAttributes['PayrollRunId']
	resultId = sessionAttributes['CurrentResultId']

	return close(
		sessionAttributes,
		'Fulfilled',
		{
			'contentType':'PlainText',
			'content': 'Issue reported by {}: {}.\nPayrollRunId is {}\nPayrollResultId is {}'.format(empId,reportContent,payrollRunId,resultId)
		}
		)

# get the details of a specific item in payroll summary
def emp_detail(intent_request):
	slots = intent_request['currentIntent']['slots']
	attribute = slots['Attribute']
	sessionAttributes = intent_request.get('sessionAttributes',{})
	resultId = sessionAttributes['CurrentResultId']
	detail = pi_mock_api.get_payroll_attribute(orgId,resultId,attribute)
	detail = format_detail(detail)
	return close(
		sessionAttributes,
		'Fulfilled',
		{
			'contentType':'PlainText',
			'content': 'Here are what included in '+ attribute + ':\n'+detail
		}
		)


# --- direct request based on intent ---
def dispatch(intent_request):

    logger.debug('dispatch userId={}, intentName={}'\
                 .format(intent_request['userId'], \
                         intent_request['currentIntent']['name']))
    intent_name = intent_request['currentIntent']['name']

    empName = intent_request['userId']
    empId = empName.split('-')[-1]

    if intent_name == 'EmployeeInfo':
        return emp_info(intent_request)
    elif intent_name == 'Compare':
        return payroll_comparison(intent_request)
    elif intent_name == 'ReportError':
        return report_error(intent_request)
    elif intent_name == 'EmployeeDetail':
    	return emp_detail(intent_request)

    raise Exception('Intent with name ' + intent_name + ' not supported')
    

# --- Main handler ---
def handler(event, context):
    
    os.environ['TZ'] = 'America/New_York'
    time.tzset()
    logger.debug('FullfillmentCodeHook:bot_name={}, bot_alias={}, bot_version={}'\
                 .format(event['bot']['name'], event['bot']['alias'],\
                         event['bot']['version']))

    return dispatch(event)