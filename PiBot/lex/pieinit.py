from __future__ import print_function
import boto3
import re
import os
import time
import datetime
import logging
import json
import pi_mock_api

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

orgId = 'AAA'
empId = '7519'
empName = ''


# --- Helpers that build all of the responses ---

def elicit_slot(session_attributes, intent_name, slots, slot_to_elicit,message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction':{
            'type': 'ElicitSlot',
            'intentName': intent_name,
            'slots': slots,
            'slotToElicit': slot_to_elicit,
            'message': message
        }
    }



def delegate(session_attributes,slots):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Delegate',
            'slots': slots
        }
    }


def close(session_attributes, fulfillment_state, message):
    response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message
        }
    }

    return response

# --- helper functions ---
def get_criteria(sessionAttributes):
    if sessionAttributes and sessionAttributes.get('CriteriaId',None):
        return sessionAttributes['CriteriaId']
    crt = pi_mock_api.get_criteria(orgId,empId)
    if not crt:
        return pi_mock_api.create_criteria(orgId,empId)
    return crt

def get_pre_date(startDate, endDate):
    startDatedt = datetime.datetime.strptime(startDate,"%Y-%m-%d")
    endDatedt = datetime.datetime.strptime(endDate,"%Y-%m-%d")
    delta = (endDatedt - startDatedt).days
    newEnd = startDatedt - datetime.timedelta(days=1)
    newStart = newEnd - datetime.timedelta(days=delta)
    return (newStart.strftime('%Y-%m-%d'), newEnd.strftime('%Y-%m-%d'))

def parse_attribute(attribute):
    if re.match('.*'+attribute+'.*','statutoryDeductions',re.IGNORECASE):
        return 'statutoryDeductions'
    elif re.match('.*'+attribute+'.*','employerContributions',re.IGNORECASE):
        return 'employerContributions'
    return None


# --- functions to handle intent requests ---
def v_emp_info(intent_request):
    slots = intent_request['currentIntent']['slots']
    startDate = slots.get('StartDate',None)
    endDate = slots.get('EndDate',None)

    logger.debug('Validating the payroll start date {} and end date {}'.format(startDate,endDate))
    sessionAttributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    if not startDate:
        return elicit_slot(sessionAttributes,
                intent_request['currentIntent']['name'],
                slots,
                'StartDate',
                {'contentType': 'PlainText', 'content': 'What is the start date?'}
                )

    if not endDate:
        return elicit_slot(sessionAttributes,
                intent_request['currentIntent']['name'],
                slots,
                'EndDate',
                {'contentType': 'PlainText', 'content': 'What is the end date?'}
                )
    
    searchCriteria = get_criteria(sessionAttributes)
    logger.debug('Search criteria : {}'.format(searchCriteria))
    sessionAttributes['CriteriaId'] = searchCriteria
    resultId = pi_mock_api.get_resultId(orgId,startDate,endDate,searchCriteria,empId)
    logger.debug('Result Id: {}'.format(resultId))

    if resultId == 'ERRDATE':
        slots['StartDate'] = None
        slots['EndDate'] = None 
        return elicit_slot(sessionAttributes,
            intent_request['currentIntent']['name'],
            slots,
            'StartDate',
            {'contentType': 'PlainText', 'content':'I can not find any payroll in the given period. Try another date'}
            )
    else:
        sessionAttributes['CurrentResultId'] = resultId
    return delegate(sessionAttributes,slots)


def v_payroll_comparison(intent_request):
    slots = intent_request['currentIntent']['slots']
    sessionAttributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    searchCriteria = get_criteria(sessionAttributes)
    sessionAttributes['CriteriaId'] = searchCriteria

    currentInfo = sessionAttributes.get('CurPeriodInfo',{})
    logger.debug('Validating the payroll comparison.')
    logger.debug('Search criteria : {}'.format(searchCriteria))
    logger.debug('currentInfo:{}'.format(currentInfo))
    if not currentInfo:
        return close(
            sessionAttributes,
            'Fulfilled',
            {
            'contentType':'PlainText',
            'content':"There is nothing to be compared with. You can start by checking a recent payroll."
            }
        )

    logger.debug('startDate:{}, endDate:{}'.format(sessionAttributes['StartDate'],sessionAttributes['EndDate']))
    newStart,newEnd = get_pre_date(sessionAttributes['StartDate'], sessionAttributes['EndDate'])
    logger.debug('newStart:{}, newEnd: {}'.format(newStart, newEnd))
    preId = pi_mock_api.get_resultId(orgId,newStart,newEnd,searchCriteria,empId)
    i = 0
    while preId == 'ERRDATE' and i < 3:
        newStart,newEnd = get_pre_date(newStart,newEnd)
        preId = pi_mock_api.get_resultId(orgId,newStart,newEnd,searchCriteria,empId)
        i += 1

    if preId == 'ERRDATE':
        return close(
            sessionAttributes,
            'Fulfilled',
            {
            'contentType':'PlainText',
            'content':"Whoops, there is no previous payroll found."
            }
        )
    sessionAttributes['PreviousResultId'] = preId
    return delegate(sessionAttributes,slots)
	
	

def v_report_error(intent_request):
    slots = intent_request['currentIntent']['slots']
    sessionAttributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    searchCriteria = get_criteria(sessionAttributes)
    logger.debug('Validating the error report.')
    logger.debug('Search criteria : {}'.format(searchCriteria))
    if not sessionAttributes or not sessionAttributes.get('PayrollRunId',{}):
        return close(
            sessionAttributes,
            'Fulfilled',
            {
            'contentType':'PlainText',
            'content':"I could not find any information about your payroll. You can start by checking your payroll by period."
            }
        )
    return delegate(sessionAttributes, slots)

def v_emp_detail(intent_request):
    slots = intent_request['currentIntent']['slots']
    attribute = slots['Attribute']
    sessionAttributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    if not attribute:
        return elicit_slot(sessionAttributes,
            intent_request['currentIntent']['name'],
            slots,
            'Attribute',
            {'contentType': 'PlainText', 'content':'Please specify an attribute for detailed information.'}
            )
    if attribute and not parse_attribute(attribute):
        slots['Attribute'] = None
        return elicit_slot(sessionAttributes,
            intent_request['currentIntent']['name'],
            slots,
            'Attribute',
            {'contentType': 'PlainText', 'content':'This is not a valid attribute. Try something else :)'}
            )
    else:
        slots['Attribute'] = parse_attribute(attribute)


    return delegate(sessionAttributes, slots)
    
    

# --- direct request based on intent ---
def dispatch(intent_request):

    logger.debug('dispatch userId={}, intentName={}'\
                 .format(intent_request['userId'], \
                         intent_request['currentIntent']['name']))
    intent_name = intent_request['currentIntent']['name']

    empName = intent_request['userId']
    empId = empName.split('-')[-1]


    if intent_name == 'EmployeeInfo':
        return v_emp_info(intent_request)
    elif intent_name == 'Compare':
        return v_payroll_comparison(intent_request)
    elif intent_name == 'ReportError':
        return v_report_error(intent_request)
    elif intent_name == 'EmployeeDetail':
        return v_emp_detail(intent_request)

    raise Exception('Intent with name ' + intent_name + ' not supported')
    

# --- Main handler ---
def handler(event, context):
    
    os.environ['TZ'] = 'America/New_York'
    time.tzset()
    logger.debug('DialogCodeHook:bot_name={}, bot_alias={}, bot_version={}'\
                 .format(event['bot']['name'], event['bot']['alias'],\
                         event['bot']['version']))

    return dispatch(event)