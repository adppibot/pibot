import requests
import json
import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# api url builder
def _url(path):
	return 'https://4q88nvhwxk.execute-api.us-east-1.amazonaws.com/prod' + path


# Create a result search criteria to specify a group of ids to look up
# Return the criteria id (string)
# orgId is the organization id (string); empId is employee id
# API details: https://4q88nvhwxk.execute-api.us-east-1.amazonaws.com/prod/api/payroll/v0/organizations/AAA/payroll-result-search-criteria
# Sample function call and output: create_criteria('AAA','4000') -> "410a08f9-0a9b-495a-bf79-fe0e8f8c717e"
def get_criteria(orgId,empId):
	# dump list to json and send it with request as payload
	payload = json.dumps(
		{"method_name":"get_criteria",
		 "content":
		 	{"empId":empId}
		 })
	rsp = requests.post(_url('/api/payroll/v0/organizations/{}/payroll-result-get-criteria'.format(orgId)), data = payload)
	logger.debug('rsp.json(): {}'.format(rsp.json))
	criteriaId = rsp.json()
	return criteriaId



def create_criteria(orgId,empId):
	# dump list to json and send it with request as payload
	payload = json.dumps(
		{"method_name":"create_criteria",
		 "content":
		 	{"empId":empId}
		 })
	rsp = requests.post(_url('/api/payroll/v0/organizations/{}/payroll-result-search-criteria'.format(orgId)), data = payload)
	logger.debug('rsp.json(): {}'.format(rsp.json))
	criteriaId = rsp.json()
	return criteriaId

# Get the meta result between the given period range and search criteria
# Only return the latest(the last one in the list) payroll run result (list of dictionaries)
# API details: https://4q88nvhwxk.execute-api.us-east-1.amazonaws.com/prod/api/payroll/v0/organizations/AAA/payroll-results-meta

def get_result_meta(orgId,periodStartDate,periodEndDate,searchCriteria):
	payload = json.dumps(
		{"method_name":"get_result_meta",
		 "content":
		 	{"periodStartDate":periodStartDate,
		 	 "periodEndDate":periodEndDate,
		 	 "criteriaId": searchCriteria}}
		)
	# v0 !!!
	rsp = requests.post(_url('/api/payroll/v0/organizations/{}/payroll-results-meta'.format(orgId)), data=payload).json()

	logger.debug('rsp: {}'.format(rsp))
	if not rsp:
		return 'ERRDATE'

	rsp = rsp['rec']

	latest = [{'employeeId':rsp['groupCriteria'],'latestMeta':rsp['resultsMetadata'][-1]}]
	return latest


def get_resultId(orgId,periodStartDate,periodEndDate,searchCriteria,employeeId=None):
	meta = get_result_meta(orgId,periodStartDate,periodEndDate,searchCriteria)
	if meta == 'ERRDATE':
		return meta

	# If employee id specified by user return the specific result id, otherwise return all available ids
	if employeeId != None:
		return [ m['latestMeta']['processingResultId'] for m in meta if m['employeeId'] == employeeId ][0]
	return [ m['latestMeta']['processingResultId'] for m in meta ]

# Get the latest payroll running ids for a list of employees according to the search criteria
# return a list of ids (list of strings)
def get_jobId(orgId,periodStartDate,periodEndDate,searchCriteria,employeeId=None):
	meta = get_result_meta(orgId,periodStartDate,periodEndDate,searchCriteria)
	if meta == 'ERRDATE':
		return meta

	if employeeId != None:
		return [ m['latestMeta']['processingJobId'] for m in meta if m['employeeId'] == employeeId ][0]
	return [ m['latestMeta']['processingJobId'] for m in meta ]
	


# Get the latest payroll processing result in detail
# Return a dictonary of employee payroll info
def get_payroll_result(orgId,resultId):
	payload = json.dumps(
		{"method_name":"get_result_detail",
		 "content":
		 	{"processingResultId":resultId}
		}
		)
	
	response = {}
	# v0 !!!
	result =requests.post(_url('/api/payroll/v0/organizations/{}/payroll-result-detail'.format(orgId)), data=payload).json()['result']
	#response['message'] = result['messages']['content']

	#response['grossPayPeriod'] = result['grossPay']['consolidatedPeriodToDate']['amount']
	response['grossPayPeriodStr'] = str(result['grossPay']['consolidatedPeriodToDate']['amount'])+' '+result['grossPay']['consolidatedPeriodToDate']['currencyCode']
	
	#response['netPay'] = result['net']['amount']
	response['netPayStr'] = str(result['net']['amount']) + ' '+result['net']['currencyCode']
	
	response['hours'] = result['hours']['consolidatedPeriodToDate']['amount']
	
	#response['nonStatutoryDeductionsPeriod'] = result['nonStatutoryDeductions']['consolidatedPeriodToDate']['amount']
	#response['nonStatutoryDeductionsPeriodStr'] = str(result['nonStatutoryDeductions']['consolidatedPeriodToDate']['amount'])+' '+result['nonStatutoryDeductions']['consolidatedPeriodToDate']['currencyCode']
	response['employerContributionsStr'] = str(result['employerContributions']['consolidatedPeriodToDate']['amount'])+' '+result['employerContributions']['consolidatedPeriodToDate']['currencyCode']
	#response['statutoryDeductionsPeriod'] = result['satutoryDeductions']['consolidatedPeriodToDate']['amount']
	response['statutoryDeductionsPeriodStr'] = str(result['statutoryDeductions']['consolidatedPeriodToDate']['amount'])+' '+result['statutoryDeductions']['consolidatedPeriodToDate']['currencyCode']
	
	response['initiatedBy'] = result['metadata']['initiatedBy']
	response['processingResultStatus'] = result['metadata']['processingResultStatus']
	response['runDate'] = result['metadata']['runDate']
	response['payDate'] = result['metadata']['payDate']
	response['periodStartDate'] = result['metadata']['periodStartDate']
	response['periodEndDate'] = result['metadata']['periodEndDate']
	response['payrollRunId'] = result['metadata']['processingJobId']

	return response

def get_payroll_attribute(orgId,resultId,attribute):
	payload = json.dumps(
		{"method_name":"get_result_detail",
		 "content":
		 	{"processingResultId":resultId}
		}
		)
		
	rsp = requests.post(_url('/api/payroll/v0/organizations/{}/payroll-result-detail'.format(orgId)), data=payload).json()['result']
	return rsp[attribute]

def handler(event, context):
	
	rsp = get_payroll_result('B8U','1e1ec88b-eb9c-441c-bd9b-5dd74b56b5b0')
	print type(rsp)
	return rsp

