'use strict';
/******************
@author Fan Yang
 ******************/

var qs = require('qs');
var AWS = require('aws-sdk');
var slack = require('slack');
var Trello = require('trello');
var config = require('./config_slack');
console.log('config: ', config);
AWS.config.region = 'us-east-1';
var dynamodb = new AWS.DynamoDB();
// toke for POST operation
var TrelloApplicationKey = config.TrelloApplicationKey;
var token_slackApp = config.token_slackApp;
var bot_id = config.bot_id;
var admin_key = config.ADP_validation_key;
console.log('admin_key: ', admin_key);

exports.handler = (event, context, callback) => {
	// print input source, JSON file
	console.log('event:    *********', event);

	// Callback inilization
	const done = (err, res) => callback(null, {
        statusCode: err ? '400' : '200',
        body: err ? (err.message || err) : JSON.stringify(res),
        headers: {
            'Content-Type': 'application/json',
        },
    });

	try {
		// get parameters from the event JSON input
		const slackMsg = qs.parse(event['body-json']);

		/******************
		validate slack event subscriptions
		******************/
		// if (slackMsg.challenge) {
		// 	callback(slackMsg.challenge);
		// }

		/******************
		button response
		******************/
		if (slackMsg.payload) {
			var actionJSONPayload = JSON.parse(slackMsg.payload);
			console.log('payload: ~~~~~~~~~: ', actionJSONPayload);
			const channel = actionJSONPayload.channel.id;
			var menu_info = actionJSONPayload.actions[0].selected_options ? actionJSONPayload.actions[0].selected_options[0] : actionJSONPayload.actions[0];
			if (menu_info.value == 'Report') {
				var attachments_data = [
					{
						"text": "Choose the issue you want to report `or` you can type your own issue by starting from 'Report $'.",
						"fallback": "Shame... buttons aren't supported in this land",
						"callback_id": "button_tutorial",
						"color": "#3AA3E3",
						"attachment_type": "default",
						"actions": [
							{
								"name": "issue_list",
								"text": "Pick an issue...",
								"type": "select",
								"options":
								[
									{
										"text": "Payroll issue",
										"value": "Pi, Report $ Parroll issue",
									},
									{
										"text": "Work time issue",
										"value": "Pi, Report $ Work time issue",
									},
									{
										"text": "Tax issue",
										"value": "Pi, Report $ Tax issue",
									}
								]
							}
						]
					}
				]
				var params_webPost = {
					token: token_slackApp,
					channel: channel,
					text: 'You can report an issue by using the menu below or directly `@` the manager to create an channel for something urgent.',
					attachments: attachments_data
				};

				// POST to slack
				slack.chat.postMessage(params_webPost, function(err, res) {
					if (err) {
						console.log('Error:', err);
					} else {
						console.log('Message sent: ', res);
					}
				});
			} else {
				var as_user = menu_info.name == '0' ? false : true;
				var params_webPost = {
					token: token_slackApp,
					channel: channel,
					text: menu_info.value,
					as_user: as_user
				};
				slack.chat.postMessage(params_webPost, function(err, res) {
					if (err) {
						console.log('Error:', err);
					} else {
						console.log('Message sent: ', res);
					}
				});
			}
		} else {
			const requestToken = slackMsg.token;
			const user = slackMsg.event.user;
			const channel = slackMsg.event.channel;

			//	************************
			//	validate and filter bad/empty messages
			//	************************
			if(!slackMsg.hasOwnProperty('token')){
				var error = new Error("Cannot process message without a TOKEN.");
				callback(error);
			} 
			/************************
			Identify User Input
			************************/
			else if (slackMsg.event.user){
				// make identification whether it is a pi command
				var inputMsg = slackMsg.event.text;
				if (inputMsg.split(' ')[0].toUpperCase() != 'PI,') {
					return;
				} else {
					inputMsg = inputMsg.split('').splice(4, inputMsg.length, '').join('');
				}

				// help menu, let users choose intent
				if (inputMsg == 'start') {
					var params_webPost = {
						token: token_slackApp,
						channel: channel,
						text: 'Hi, I am Pi Innovation Bot from NextGen Payroll, ADP, LLC. \n I can help you check your `payroll status`, give you `payroll alerts` or help you `report an issue` to the manager',
						attachments: [
									{
										"text": "Choose your intent please:",
										"fallback": "Shame... buttons aren't supported in this land",
										"callback_id": "button_tutorial",
										"color": "#3AA3E3",
										"attachment_type": "default",
										"actions": [
											{
												"name": 1,
												"text": "Payroll Summary",
												"type": "button",
												"value": "Pi, Show me my payroll summary",
												"style": "primary"
											},
											{
												"name": 1,
												"text": "Compare Payroll",
												"type": "button",
												"value": "Pi, How does it compare to last time"
											},
											{
												"name": 0,
												"text": "Trello List Setup",
												"type": "button",
												"value": "If you are a manager and you want to push the employee's issues to your `Trello list`, please use the 'admin key' provided by ADP and application key ( `5dfa9d006d1b6e134fb7c5c4a12a36d7` ) to generate your `Trello token` and `list ID` and input as \n >`admin_key=Trello_token=list_ID` "
											},
											{
												"name": 0,
												"text": "Report Issue",
												"type": "button",
												"value": "Report",
												"style": "danger"
											}
										]
									}
								]
						};
					slack.chat.postMessage(params_webPost, function(err, res) {
						if (err) {
							console.log('Error:', err);
						} else {
							console.log('Message sent: ', res);
						}
					});
					
				}
				/************************
				Group bot, manager and user
				************************/
				else if (inputMsg.match('@')) {
					// if we @ someone in the channel, we create a new channel and invite all the people who is pinned
					// get the list of the users

					console.log('match: ', inputMsg.match(/@/g).length);
					var user_num = inputMsg.match(/@/g).length;
					var user_list = inputMsg.replace(/["<>@"]/g, '').split(" ");
					if (user_num == user_list.length) {
						console.log('err: ', 'not match');
						user_list.push(slackMsg.event.user);
						user_list.push(bot_id);
						var name_create = 'pii_channel_' + 10000*(Math.random().toFixed(4));
						var params_channel_create = {
							token: token_slackApp,
							name: name_create,
						}

						slack.channels.create(params_channel_create, function(err, info){
							console.log('channel_create: ', info);
							if (err) {
								console.log('err: ', err);
							} else {
								var channel_create_id = info.channel.id;
								for (var i =  0; i < user_list.length; i++) {
									var params_invite = {
										token: token_slackApp,
										channel: channel_create_id,
										user: user_list[i]
									}
									slack.channels.invite(params_invite, function(err, info){});
								}
							}
						});
					}
				} 
				/************************
				Trello set up
				************************/
				else if (inputMsg.match(/["="]/)) {
					var split_key = inputMsg.split(/["="]/);
					if (split_key.length != 3) {
						var params_webPost = {
							token: token_slackApp,
							channel: channel,
							text: 'Sorry, wrong input to set up trello!'
							};
						slack.chat.postMessage(params_webPost, function(err, res) {
							if (err) {
								console.log('Error:', err);
							} else {
								console.log('Message sent: ', res);
							}
						});
					} else if (split_key[0] != admin_key) {
						var params_webPost = {
							token: token_slackApp,
							channel: channel,
							text: 'Sorry, wrong manager key to set up trello!'
							};
						slack.chat.postMessage(params_webPost, function(err, res) {
							if (err) {
								console.log('Error:', err);
							} else {
								console.log('Message sent: ', res);
							}
						});
					} else {
						var docClinet = new AWS.DynamoDB.DocumentClient();
						var table = "PiInnovationBot_TrelloAdmin";
						var teamID = slackMsg.team_id;
						var params_trello_admin = {
							TableName: table,
							Item: {
								"TeamID": teamID,
								"TrelloUserToken": split_key[1],
								"myListId": split_key[2]
							}
						}
						docClinet.put(params_trello_admin, function(err, data) {
							var text = '';
							if (err) {
								text = 'Unable to access DynamoDB for trello admin.';
								console.error("Unable to access DynamoDB for trello admin.");
							} else {
								text = 'Added trello account';
								console.log("Added trello account", data);
							}
							var params_webPost = {
								token: token_slackApp,
								channel: channel,
								text: text
							};
							slack.chat.postMessage(params_webPost, function(err, res) {
								if (err) {
									console.log('Error:', err);
								} else {
									console.log('Message sent: ', res);
								}
							});
						});
					}
				}
				/************************
				report issue and store in DynamoDB
				************************/
				else if (inputMsg.match(/["$"]/)) {
					var report_text = inputMsg.split(/["$"]/)[1];
					// check whether this team has a trello list
					var docClinet = new AWS.DynamoDB.DocumentClient();
					var table = "PiInnovationBot_TrelloAdmin";
					var teamID = slackMsg.team_id;
					var params_check_team_trello = {
						TableName: table,
						Key: {
							TeamID: teamID
						}
					};

					docClinet.get(params_check_team_trello, function(err, data) {
						if (!data.Item) {
							console.log('channel: ', channel);
							var params_webPost = {
								token: token_slackApp,
								channel: channel,
								text: 'Sorry, your manager did not use trello, you can contact him/her directly by @ him/her.'
							};
							console.log('err_dynamo: ', params_webPost);
							slack.chat.postMessage(params_webPost, function(err, res) {
								if (err) {
									console.log('Error:', err);
								} else {
									console.log('Message sent: ', res);
								}
							});
						} else {
							console.log('err_dynamo: ', data);
							var trello = new Trello(TrelloApplicationKey, data.Item.TrelloUserToken);

							var params_user = {
								token: token_slackApp,
								user: user,
							};
							slack.users.info(params_user, function(err, info) {
								if (err) {
									console.log('err', err);
								} else {
									// add card to trello
									var date_now = new Date().toLocaleString();
									var title = info.user.profile.real_name + ' - ' + report_text;
									trello.addCard(title, date_now , data.Item.myListId, function (error, trelloCard) {
										var text = '';
										if (error) {
											text = 'Could not report.';
										}
										else {
											text = 'Reported to the manager.';
										}
										var params_webPost = {
											token: token_slackApp,
											channel: channel,
											text: text
											};
										slack.chat.postMessage(params_webPost, function(err, res) {});
									});
									
									// add record to DynamoDB 
									var docClinet2 = new AWS.DynamoDB.DocumentClient();
									var table = "PiInnovationBot_ReportedIssues";
									var teamID = slackMsg.team_id;
									var params_reported_record = {
										TableName: table,
										Item: {
											"userID": info.user.profile.real_name,
											"date": date_now,
											"Info": {
												"teamId:": teamID,
												"report_text": report_text,
											}
										}
									}
									docClinet2.put(params_reported_record, function(err, data) {});
								}
							});
							
						}
					});

				} 
				/************************
				Iterative messaging with lex
				************************/ 
				else {
					// get the intent
					var lexruntime = new AWS.LexRuntime();
					var params_user = {
						token: token_slackApp,
						user: user,
					};
					slack.users.info(params_user, function(err, info) {
						console.log('user_info: ', info);
						if (err) {
							console.log('err', err);
						} else {
							const user_info = info.user.profile.real_name;
							// console.log('sessionAttributes_value: ', sessionAttributes_value);
							var params = {
								botAlias: process.env.BOT_ALIAS,
								botName: process.env.BOT_NAME,
								inputText: slackMsg.event.text,
								userId: user_info,
							};

							// send the info to Lex and retrive response and POST the response back to slack
							lexruntime.postText(params, function(err, data) {
								console.log('data: ', data);
								if (err) {
									console.log('error: ',err); 
									var msg = 'Sorry we catch an error!';
									callback(err, msg);
						
								} else {
									// got something back from Amazon Lex
									console.log('data-json: ', data);
									// sessionAttributes_value = data.sessionAttributes;
									var attachments_data = [];
									
									var params_webPost = {
										token: token_slackApp,
										channel: channel,
										text: data.message,
										attachments: attachments_data
									};

									// POST to slack
									slack.chat.postMessage(params_webPost, function(err, res) {
										if (err) {
											console.log('Error:', err);
										} else {
											console.log('Message sent: ', res);
										}
									});
								}
							});
						}
					});

					
				}
					
			}
		}
    	
	} catch(e) {
		console.log(e);
		callback(e);
	}
};
