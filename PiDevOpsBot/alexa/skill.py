from __future__ import print_function
import boto3
import os
import time
import datetime
import logging
import jenkins
import json
import re
import xml.etree.ElementTree as ET

#----global variables-----
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
server_url = 'http://jenkins.ilovethaitea.com:8080'
# --- create a global Jenkins instance
server = jenkins.Jenkins(server_url)
logger.debug("Connected to jenkins server")
version = server.get_version()
logger.debug("Jenkins version {}".format(version))


dashboard_list = []

for v in server.get_views():
    dashboard_list.append(v['name'])


status_list = {'success':['success','successful','succeeded'],\
                'failure':['failure','failed'],\
                'aborted':['aborted'],\
                'other_options':['last successful build time', 'last successful build time',\
                'last build time','last build number',\
                'sub-builds status, all sub-builds status',\
                'url']}

valid_buildCode_list = {'last build','last failed build','last unsuccessful build','last successful build'} 
valid_buildAction_list = {'triggered','executed','executor','claimed'}

#----validation helper functions---

def valid_dashboard_name(dashboard_name):
	if dashboard_name in dashboard_list:
		return True
	return False

def valid_dashStatus(dash_status):
    if (dash_status in status_list['success']) or (dash_status in status_list['failure']) or (dash_status in status_list['aborted']) or dash_status == 'all':
        return True
    return False

def valid_job_index(job_list, job_index):
	if job_list.get(job_index,{}):
		return True
	return False

def valid_info_key(info_key):
    if (info_key in status_list['other_options']) or (info_key in status_list['success']\
        ) or (info_key in status_list['failure']) or (info_key in status_list['aborted']):
        return True
    return False


def valid_buildCode(build_code):
	if build_code in valid_buildCode_list:
		return True
	return False

#----fullfillment helper functions---

def build_speechlet_response(title,output,reprompt_text,should_end_session):
	return {
		'outputSpeech': {
			'type': 'SSML',
			'ssml': output
		},
		'card':{
			'type':'Simple',
			'title': 'SessionSpeechlet - ' + title,
			'content': 'SessionSpeechlet - ' + output
		},
		'reprompt':{
			'outputSpeech':{
				'type': 'PlainText',
				'text': reprompt_text
			}
		},
		'shouldEndSession': should_end_session
	}

def build_response(session_attributes, speechlet_response):
	return {
		'version': '1.0',
		'sessionAttributes': session_attributes,
		'response': speechlet_response
	}


def parse_viewName(view_name):
    view_name = view_name.lower()
    pattern = '.*'+view_name+'.*'
    p = re.compile(pattern,flags=re.IGNORECASE)
    for d in dashboard_list:
        if p.match(d) != None:
            return d
    return None

def get_job_list(view_name):
    result = []
    for v in view_list:
        view_config = server.get_view_config(v)
        tree = ET.ElementTree(ET.fromstring(view_config))
        root = tree.getroot()
        if v == root.find('name').text:
            for job in root.iter('string'):
                result.append(job.text)
        
    return result

def get_job_list_view(view_name):
    view_config = server.get_view_config(view_name)
    tree = ET.ElementTree(ET.fromstring(view_config))
    root = tree.getroot()
    result = []
    if view_name == root.find('name').text:
        for job in root.iter('string'):
            result.append(job.text)
    return result


def get_executor(b_actions):
    for a in b_actions:
        if 'causes' in a:
            if 'userName' in a['causes'][0]:
                return '<speak> Executor: {},<break strength="medium"/> userId: {} </speak>'.format(a['causes'][0]['userName'], a['causes'][0]['userId'])
            elif 'upstreamProject' in a['causes'][0]:
                return '<speak> Cause: '+a['causes'][0]['shortDescription'] + '</speak>'
    return '<speak> No execution record found.</speak>'


def get_claimer(b_actions):
    for a in b_actions:
        if 'claimed' in a:
            return '<speak> Claimed by: {} on {},<break strength="medium"/> reason: {} </speak>'.format(a['claimedBy'], tsTodt(a['claimDate']), a['reason'])
    return '<speak>This build was not claimed.</speak>'


def get_status(job_name):
    stat = {}
    job_info = server.get_job_info(job_name)
    n_last_build = job_info['lastBuild']['number']
    build_info = server.get_build_info(job_name,n_last_build)
    stat['n_last_build'] = n_last_build
    stat['result'] = build_info['result']
    stat['executor'] = get_executor(build_info['actions'])
    stat['claimer'] = get_claimer(build_info['actions'])
    stat['url'] = job_info['url']
    return stat

def get_build_info(job_name,build_action,build_code):
    binfo = server.get_build_info(job_name,build_code)
    b_actions = binfo['actions']
    result = "<speak>Nothing found</speak>"
    logger.debug("build_code:{},build_info['actions']:{}".format(build_code,b_actions))

    if build_action == 'causes':
   		result = get_executor(b_actions)
    elif build_action == 'claimed':
    	result = get_claimer(b_actions)
    return result


def get_simple_list(view_name,dash_status):
	job_list = get_job_list_view(view_name)
	name_list ='<speak>'
	for job in job_list:
		stat = get_status(job)
		result = stat['result']
		if dash_status == 'all':
			name_list += str(job_list.index(job)+1)+". "
			name_list += '<break strength="medium"/>'
			name_list += '<s>{}\n'.format(job.replace('_',' '))
			name_list += '</s>'
		elif dash_status == 'successful' and result != None:
			if result.lower() == 'success':
				name_list += str(job_list.index(job)+1)+". "
				name_list += '<break strength="medium"/>'
				name_list += '<s>{}\n'.format(job.replace('_',' '))
				name_list += '</s>'
		elif dash_status == 'failed' and result != None:
			if result.lower() == 'failure':
				name_list += str(job_list.index(job)+1)+". "
				name_list += '<break strength="medium"/>'
				name_list += '<s>{}\n'.format(job.replace('_',' '))
				name_list += '</s>'
	if name_list == '<speak>':
		name_list  = 'No job found'

	name_list += '</speak>'
	return name_list


def tsTodt(timestamp):
	if timestamp == None:
		return "no timestamp found"
	result = datetime.datetime.fromtimestamp(timestamp/1000)
	return result


def get_info_job(job_name, info_key):
    logger.debug('start getting info....')
    job_info = server.get_job_info(job_name)
    logger.debug('getting {} of job {}'.format(info_key,job_name))
    result = '<speak>'
    if info_key == 'last successful build number':
        result += str(job_info['lastSuccessfulBuild']['number'])
    elif info_key == 'last build number':
        result += str(job_info['lastBuild']['number'])
    elif info_key == 'last successful build time':
        build_info = server.get_build_info(job_name,\
            job_info['lastSuccessfulBuild']['number'])
        result += str(tsTodt(build_info['timestamp']))
    elif info_key == 'url':
        result += str(job_info['url'])
        

    return result+'</speak>'

def parse_build_code(build_code):
    if build_code == "last build":
        return 'lastBuild'
    elif build_code == 'last failed build':
        return 'lastFailedBuild'
    elif build_code == 'last unsuccessful build':
        return 'lastUnsuccessfulBuild'
    elif build_code == 'last successful build':
        return 'lastSuccessfulBuild'


def parse_build_action(build_action):
    if build_action == 'triggered' or build_action == 'executed' or build_action == 'executor':
        return 'causes'
    else:
        return 'claimed'


#----event function----

def get_help_response():
	card_title = 'Help'
	session_attributes = {}
	should_end_session = False
	speech_output = "<speak>You can ask me to list you 'all','failed','successful' jobs in a\
					specific dashboard. <s>Then you can ask me for the detail of any of \
					the job by referencing their index.</s> See Help card for more information.</speak>"
	return build_response(session_attributes,build_speechlet_response(
		card_title,speech_output,'',should_end_session))

def get_welcome_response():
	session_attributes = {}
	card_title = "Welcome"
	speech_output = "<speak>Welcome to jenkins chatbot."\
					"<s>You can check the dashboard by name and job by index in the dashboard.</s></speak>"
	reprompt_text = "You can try Go to Integration Dashboard."
	should_end_session = False
	return build_response(session_attributes,build_speechlet_response(
		card_title,speech_output,reprompt_text,should_end_session))

def handle_session_end_request():
	card_title = 'Session Ended'
	speech_output = 'Bye'
	should_end_session = True
	return build_response({},build_speechlet_response(
		card_title, speech_output, None, should_end_session))

def query_dashboard(intent,session):
	card_title = intent['name']
	session_attributes = {}
	should_end_session = False

	if session.get('attributes',{}):
		session_attributes = session['attributes']

	

	#validate slot DashboardName, one of the shared variable that could be in session_attribute.
	if 'DashboardName' in intent['slots'] and valid_dashboard_name(parse_viewName(intent['slots']['DashboardName']['value'])):
		dashboard_name = parse_viewName( intent['slots']['DashboardName']['value'] )
		session_attributes['DashboardName'] = dashboard_name
		logger.debug("Got valid dashboard name from user: {}".format(dashboard_name))
	elif session_attributes.get('DashboardName',{}) and valid_dashboard_name(session_attributes['DashboardName']):
		dashboard_name = session_attributes['DashboardName']
		logger.debug("Got valid dashboard name from session: {}".format(dashboard_name))
	else:
		logger.debug("No valid dashboard name got")
		logger.debug('DashboardName in slots: {}'.format('DashboardName' in intent['slots']) )
		speech_output = '<speak>I\'m not sure about the dashboard name.<break strength="medium"/>'\
						"Please try again</speak>"
		reprompt_text = "You can check dashboards names on card."

		return build_response(session_attributes,build_speechlet_response(
			card_title,speech_output,reprompt_text,should_end_session))


	#validate slot DashStatusKey. Required slot input.
	if 'DashStatusKey' in intent['slots'] and valid_dashStatus(intent['slots']['DashStatusKey']['value']):
		dash_status = intent['slots']['DashStatusKey']['value']
		logger.debug("Got valid dashboard status from user: {}".format(dash_status))
	else:
		logger.debug("No valid dashboard status got")
		logger.debug('Dashboard status in slots: {}'.format('DashStatusKey' in intent['slots']) )
		if 'DashStatusKey' in intent['slots']:
			logger.debug('DashStatusKey: {}'.format(intent['slots']['DashStatusKey']['value'] ))
		speech_output = "<speak>I'm not sure about the status you want ."\
						"Please try again</speak>"
		reprompt_text = "You can check dashboards by saying list all or failed or successful jobs."

		return build_response(session_attributes,build_speechlet_response(
			card_title,speech_output,reprompt_text,should_end_session))

	result = get_simple_list(dashboard_name,dash_status)
	
	job_list = get_job_list_view(dashboard_name)
	dic = {}
	for job in job_list:
		dic[str(job_list.index(job))] = job
	session_attributes['JobListStr'] = result
	session_attributes['JobList'] = json.dumps(dic)

	return build_response(session_attributes,build_speechlet_response(card_title,result,None,should_end_session))


def query_job(intent,session):
	card_title = intent['name']
	session_attributes = {}
	should_end_session = False
	if session.get('attributes',{}):
		session_attributes = session['attributes']

	job_list = session_attributes.get('JobList',{})
	if job_list:
		job_list = json.loads(job_list)
	logger.debug("Got job list from session_attribute: {}".format(job_list))
	
	
	if job_list == {}:
		speech_output = "<speak><s>I don't have any job in the list.</s>"\
						"Please specify the dashboard you want to look at first.</speak>"


	if 'JobIndex' in intent['slots'] and intent['slots']['JobIndex'].get('value',{}) and valid_job_index(job_list,intent['slots']['JobIndex']['value']):
		job_index = intent['slots']['JobIndex']['value']
		logger.debug("job_index in list: {}".format(job_index in job_list))
		job_name = job_list[job_index]
		logger.debug("Got valid job index from user: {}, job name: {}".format(job_index, job_name))
		session_attributes['JobName'] = job_name
	elif session_attributes.get('JobName',{}):
		job_name = session_attributes['JobName']
		logger.debug("Got valid job name from session: {}".format(job_name))
	else:
		logger.debug("No valid job index got")
		speech_output = '<speak>Invalid job index.<break strength="medium"/>'\
						'Please try again</speak>'
		job_list_str = session_attributes['JobListStr'] 
		reprompt_text = "The list of jobs are {}. You can check them by index.".format(job_list_str)
		return build_response(session_attributes,build_speechlet_response(
			card_title,speech_output,reprompt_text,should_end_session))


	if 'JobInfoKey' in intent['slots'] and valid_info_key(intent['slots']['JobInfoKey']['value'] ):
		job_info_key = intent['slots']['JobInfoKey']['value']
		logger.debug("Got valid job info key from user: {}".format(job_info_key))
	else:
		logger.debug("No valid job info key got")
		speech_output = "<speak>Invalid query key."\
						"Please try again</speak>"
		reprompt_text = "You can ask the last build number or last successful build time of this job."
		return build_response(session_attributes,build_speechlet_response(
			card_title,speech_output,reprompt_text,should_end_session))

	result = get_info_job(job_name,job_info_key)

	return build_response(session_attributes,build_speechlet_response(card_title,result,None,should_end_session))


def query_build(intent,session):
	card_title = intent['name']
	session_attributes = {}
	should_end_session = False
	if session.get('attributes',None):
		session_attributes = session['attributes']
		job_list = session_attributes.get('JobList',None)
	
	
	if job_list == None:
		speech_output = "<speak>I don't have any job in the list."\
						"<s>Please specify the dashboard you want to look at first.</s></speak>"

	logger.debug('JobIndex in slots: {}'.format('JobIndex' in intent['slots']))

	if 'JobIndex' in intent['slots'] and intent['slots']['JobIndex'].get('value',{}) and valid_job_index(job_list,intent['slots']['JobIndex']['value'] ):
		job_index = intent['slots']['JobIndex']['value']
		job_name = job_list[job_index]
		logger.debug("Got valid job index from user: {}, job name: {}".format(job_index, job_name))
		session_attributes['JobName'] = job_name
	elif session_attributes.get('JobName',{}):
		job_name = session_attributes['JobName']
		logger.debug("Got valid job name from session: {}".format(job_name))
	else:
		logger.debug("No valid job index got")
		speech_output = "<speak>Invalid job index."\
						"Please try again</speak>"
		job_list_str = session_attributes['JobListStr'] 
		reprompt_text = "The list of jobs are {}. You can check them by index.".format(job_list_str)
		return build_response(session_attributes,build_speechlet_response(
			card_title,speech_output,reprompt_text,should_end_session))


	if 'BuildAction' in intent['slots'] and intent['slots']['BuildAction']['value'] in valid_buildAction_list:
		build_action = parse_build_action(intent['slots']['BuildAction']['value'])
		logger.debug("Got valid build action from user: {}".format(build_action))
	else:
		logger.debug("No valid build action got")
		speech_output = "<speak>I'm not sure the action you are asking about."\
						"Please try again</speak>"
		reprompt_text = "You can ask something like who triggered the last failed job of this job."
		return build_response(session_attributes,build_speechlet_response(
			card_title,speech_output,reprompt_text,should_end_session))


	if 'BuildCode' in intent['slots'] and valid_buildCode( intent['slots']['BuildCode']['value']):
		build_code = intent['slots']['BuildCode']['value']
		logger.debug("Got valid build code from user: {}".format(build_code))
	else:
		logger.debug("No valid job build code")
		speech_output = "<speak>I'm not sure the build you are asking about."\
						"Please try again</speak>"
		reprompt_text = "You can ask something like who triggered the last failed build of this job."
		return build_response(session_attributes,build_speechlet_response(
			card_title,speech_output,reprompt_text,should_end_session))

	if build_code.isdigit():
		build_code = int(build_code)
	else:
		jinfo = server.get_job_info(job_name)
		build_code = int(jinfo[parse_build_code(build_code)]['number'])

	result = get_build_info(job_name,build_action,build_code)
	return build_response(session_attributes,build_speechlet_response(card_title,result,None,should_end_session))


#----events----

def on_session_started(session_started_request, session):
	logger.debug("on_started requestId=".format(session_started_request['requestId']))
	logger.debug("sessionId=".format(session['sessionId']))

def on_launch(launch_request,session):
	logger.debug("on_launch requestId=".format(launch_request['requestId']))
	logger.debug("sessionId=".format(session['sessionId']))

	return get_welcome_response()

def on_intent(intent_request, session):
	logger.debug("on_intent requestId=".format(intent_request['requestId']))
	logger.debug("sessionId=".format(session['sessionId']))

	intent = intent_request['intent']
	intent_name = intent_request['intent']['name']

	if intent_name == 'AMAZON.HelpIntent':
		return get_help_response()
	elif intent_name == 'AMAZON.CancelIntent' or intent_name == 'AMAZON.StopIntent':
		return handle_session_end_request()
	elif intent_name == 'QueryDashBoard':
		return query_dashboard(intent,session)
	elif intent_name == 'QueryJob':
		return query_job(intent,session)
	elif intent_name == 'QueryBuild':
		return query_build(intent,session)
	else:
		raise ValueError('Invalid intent')

def on_session_ended(session_ended_request,session):
	logger.debug("on_session_ended requestId=".format(session_ended_request['requestId']))
	logger.debug("sessionId=".format(session['sessionId']))


#----main handler----

def handler(event, context):
	logger.debug('applicationId:{}'.format(event['session']['application']['applicationId']))
	logger.debug('userId:{}'.format(event['session']['user']['userId']))

	if event['session']['new']:
		on_session_started({'requestId':event['request']['requestId']},event['session'])


	if event['request']['type'] == 'LaunchRequest':
		return on_launch(event['request'],event['session'])
	elif event['request']['type'] == 'IntentRequest':
		return on_intent(event['request'],event['session'])
	elif event['request']['type'] == 'SessionEndedRequest':
		return on_session_ended(event['request'],event['session'])


