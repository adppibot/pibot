from __future__ import print_function
import boto3
import os
import time
import datetime
import logging
import jenkins
import re
import xml.etree.ElementTree as ET

# --- global variables ---
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
server_url = 'http://jenkins.ilovethaitea.com:8080/'


check_color_list = ['red','blue','aborted']
server_color_list = ['red','blue','aborted']
#check_build_status_list = ['success','failure','aborted','successful','failed']

status_list = {'success':['success','successful','succeeded'],\
                'failure':['failure','failed'],\
                'aborted':['aborted'],\
                'other_options':['last successful build time', 'last successful build time',\
                'last build time','last build number',\
                'sub-builds status, all sub-builds status',\
                'url']}

valid_buildCode_list = {'last build','last failed build','last unsuccessful build','last successful build'} 
valid_buildQuery_list = {'triggered','executed','executor','claimed'}

valid_dashQuery_list = {'list'}

# --- create a global Jenkins instance
#server = jenkins.Jenkins(server_url)
server = jenkins.Jenkins(server_url)
logger.debug("Connected to jenkins server")
version = server.get_version()
logger.debug("Jenkins version {}".format(version))


# list of all views as dashboard names on jenkins server
dashboard_list = []

for v in server.get_views():
    dashboard_list.append(v['name'])


# --- Helpers that build all of the responses ---
def elicit_slot(session_attributes, intent_name, slots, slot_to_elicit,message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction':{
            'type': 'ElicitSlot',
            'intentName': intent_name,
            'slots': slots,
            'slotToElicit': slot_to_elicit,
            'message': message
        }
    }



def delegate(session_attributes,slots):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Delegate',
            'slots': slots
        }
    }



# --- helper functions ---

def get_job_list(view_list):
    result = []
    for v in view_list:
        view_config = server.get_view_config(v)
        tree = ET.ElementTree(ET.fromstring(view_config))
        root = tree.getroot()
        if v == root.find('name').text:
            for job in root.iter('string'):
                result.append(job.text)
        
    return result

######
job_name_list = get_job_list(dashboard_list)
total_job_num = len(job_name_list)
logger.debug("Got job name list, total job number is {}".format(total_job_num))
######



def try_ex(func):
   
    try:
        return func()
    except KeyError:
        return None

def valid_checkKey(check_key):
    if (check_key in check_color_list) or (check_key in status_list['other_options']) or (check_key in status_list['success']\
        ) or (check_key in status_list['failure']) or (check_key in status_list['aborted']):
        return True
    return False

def valid_jobName(job_name):
    logger.debug("job_name type:{}".format(type(job_name)))
    if job_name in job_name_list or job_name.isdigit():
        return True
    return False

def valid_buildCode(job_name,build_code):
    jinfo = server.get_job_info(job_name)
    n_lastBuild = jinfo['lastBuild']['number']
    if build_code.isdigit():
        build_code = int(build_code)
        if build_code <= n_lastBuild and build_code > 0:
            return True
    else:
        if build_code in valid_buildCode_list:
            return True
    return False


def valid_buildQuery(build_query):
    if build_query in valid_buildQuery_list:
        return True
    return False

def valid_dashStatus(dash_status):
    if (dash_status in status_list['success']) or (dash_status in status_list['failure']) or (dash_status in status_list['aborted']) or dash_status == 'all':
        return True
    return False

def parse_viewName(view_name):
    view_name = view_name.lower()
    pattern = '.*'+view_name+'.*'
    p = re.compile(pattern,flags=re.IGNORECASE)
    for d in dashboard_list:
        if p.match(d) != None:
            return d
    return None

def parse_jobName(job_name):
    job_name = job_name.lower()
    pattern = '.*'+job_name+'.*'
    p = re.compile(pattern,flags=re.IGNORECASE)
    for j in job_name_list:
        if p.match(j) != None:
            logger.debug("replace name {} with name {}".format(job_name,j))
            return j
    return None

# --- functions to handle intent requests ---
def vq_all_status(intent_request):

    slots = intent_request['currentIntent']['slots']
    #status either successful or failed
    check_key = try_ex(lambda:slots['CheckKey'])
    logger.debug('validating check key: {}'.format(check_key))
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    #source = intent_request['invocationSource']

    new_slot = intent_request['currentIntent']['slots']

    if check_key and not valid_checkKey(check_key):
        logger.debug("Invalid checkKey:{}".format(check_key))
        
        new_slot['CheckKey'] = None

        return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            new_slot,
                            'CheckKey',
                            {'contentType': 'PlainText', 'content':'"{}" is not a valid check key. Please specify another one.'.format(check_key)}
                            )
    return delegate(session_attributes,slots)

    

   
    
def vq_job(intent_request):
    slots = intent_request['currentIntent']['slots']
    job_name = try_ex(lambda:slots['JobName'])
    check_key = try_ex(lambda:slots['CheckKey'])
    logger.debug('validating job name: {}'.format(job_name))
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    new_slot = intent_request['currentIntent']['slots']

    if job_name and job_name.isdigit():
        job_name = try_ex(lambda:session_attributes[job_name])



    if job_name == None:
        job_name = try_ex(lambda:session_attributes['JobName'])
        logger.debug("Attempted to retrieve job_name from session_attributes:{}".foramt(job_name))
        
    slots['JobName'] = job_name

    if check_key and not valid_checkKey(check_key):
        logger.debug("Invalid checkKey:{}".format(check_key))
        new_slot['CheckKey'] = None

        return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            new_slot,
                            'CheckKey',
                            {'contentType': 'PlainText', 'content':'"{}" is not a valid check key.Please specify another one.'.format(check_key)}
                            )

    if job_name and not valid_jobName(job_name):
        job_name = parse_jobName(job_name)
        if job_name == None or not valid_jobName(job_name):
            new_slot['JobName'] = None
            logger.debug("Invalid jobName:{}".format(job_name))

            return elicit_slot(session_attributes,
                                intent_request['currentIntent']['name'],
                                new_slot,
                                'JobName',
                                {'contentType': 'PlainText', 'content': '"{}" is not a valid job name.Please specify another one'.format(job_name)}
                                 )
        else:
            slots['JobName'] = job_name

    if not job_name:
        logger.debug("No job name provided.")
        return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            slots,
                            'JobName',
                            {'contentType': 'PlainText', 'content': 'Please specify the job name.'}
                            )


    return delegate(session_attributes,slots)
    
def vq_build(intent_request):
    slots = intent_request['currentIntent']['slots']
    job_name = try_ex(lambda:slots['JobName'])
    build_code  = try_ex(lambda:slots['BuildCode'])
    build_query = try_ex(lambda:slots['BuildQueryKey'])
    logger.debug('validating job name: {}'.format(job_name))
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    new_slot = intent_request['currentIntent']['slots']   

    if job_name and job_name.isdigit():
        job_name = try_ex(lambda:session_attributes[job_name]) 
    if job_name == None:
        job_name = try_ex(lambda:session_attributes['JobName'])
        debug.logger("Attempted to retrieve job_name from session_attributes:{}".foramt(job_name))
    
    slots['JobName'] = job_name


    if job_name and not valid_jobName(job_name):
        job_name = parse_jobName(job_name)
        if job_name == None or not valid_jobName(job_name):
            new_slot['JobName'] = None
            logger.debug("Invalid jobName:{}".format(job_name))

            return elicit_slot(session_attributes,
                                intent_request['currentIntent']['name'],
                                new_slot,
                                'JobName',
                                {'contentType': 'PlainText', 'content': '"{}" is not a valid job name.Please specify another one'.format(job_name)}
                                 )
        else:
            slots['JobName'] = job_name

    if not job_name:
        logger.debug("No job name provided.")
        return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            slots,
                            'JobName',
                            {'contentType': 'PlainText', 'content': 'Please specify the job name.'}
                            )

    if build_code and not valid_buildCode(job_name,build_code):
        logger.debug("Invalid buildCode:{}".format(build_code))
        new_slot['BuildCode'] = None

        return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            new_slot,
                            'BuildCode',
                            {'contentType': 'PlainText',
                            'contetnt':'"{}" is not a valid build number/code.Please specify another build code.'.format(build_code)})

    if build_query and not valid_buildQuery(build_query):
        logger.debug("Invalid buildQuery:{}".format(build_query))
        new_slot['BuildQueryKey'] = None

        return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            new_slot,
                            'BuildQueryKey',
                            {'contentType': 'PlainText',
                            'contetnt':'"{}" is not a valid build query key.Please specify the thing you want to check.'.format(build_query)}
                            )

    return delegate(session_attributes,slots)
    


def vq_view(intent_request):
    slots = intent_request['currentIntent']['slots']
    dashboard_name = try_ex(lambda:slots['DashboardName'])
    dash_query = try_ex(lambda:slots['DashQueryKey'])
    dash_status = try_ex(lambda:slots['DashStatusKey'])
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    if dash_status == None:
        slots['DashStatusKey'] = 'all'

    if dash_query:
        dash_query = dash_query.lower()
        slots['DashQueryKey'] = dash_query

    if dashboard_name == None:
        dashboard_name = try_ex(lambda:session_attributes['DashboardName'])
        slots['DashboardName'] = dashboard_name

    
    new_slot = intent_request['currentIntent']['slots']
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}


    logger.debug('validating view: {}'.format(dashboard_name))


    if dashboard_name and (not dashboard_name in dashboard_list):
        dashboard_name = parse_viewName(dashboard_name)
        logger.debug('dashboard name parsed to {}'.format(dashboard_name))

        if not dashboard_name in dashboard_list:
            logger.debug("Invalid dashboardName:{}".format(dashboard_name))

            new_slot['DashboardName'] = None


            return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            new_slot,
                            'DashboardName',
                            {'contentType': 'PlainText', 'content':'"{}" is not a valid dashboard name.Please specify another one.'.format(dashboard_name)}
                            )
        else:
            slots['DashboardName'] = dashboard_name
    if not dashboard_name:
        logger.debug("No dashboard name provided.")
        return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            slots,
                            'DashboardName',
                            {'contentType': 'PlainText', 'content': 'Please specify the dashboard name.'}
                            )

    if dash_query and not dash_query in valid_dashQuery_list:
        new_slot['DashQueryKey'] = None
        logger.debug("Invalid DashQueryKey:{}".format(dash_query))

        return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            new_slot,
                            'DashQueryKey',
                            {'contentType': 'PlainText',
                            'contetnt':'"{}" is not a valid dashboard query.You can try list all jobs.'.format(dash_query)})

    if dash_status and not valid_dashStatus(dash_status):
        new_slot['DashStatusKey'] = None
        logger.debug("Invalid dashStatus:{}".format(dash_status))

        return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            new_slot,
                            'DashStatusKey',
                            {'contentType': 'PlainText',
                            'contetnt':'"{}" is not a valid build status.Please try "failed" or "successful".'.format(dash_status)})

    return delegate(session_attributes,slots)





# --- direct request based on intent ---
def dispatch(intent_request):

    logger.debug('dispatch userId={}, intentName={}'\
                 .format(intent_request['userId'], \
                         intent_request['currentIntent']['name']))
    intent_name = intent_request['currentIntent']['name']

    if intent_name == 'QueryAllStatus':
        return vq_all_status(intent_request)
    elif intent_name == 'QueryJob':
        return vq_job(intent_request)
    elif intent_name == 'QueryBuild':
        return vq_build(intent_request)
    elif intent_name == 'QueryDashBoard':
        return vq_view(intent_request)

    raise Exception('Intent with name ' + intent_name + ' not supported')
    

# --- Main handler ---
def handler(event, context):
    
    os.environ['TZ'] = 'America/New_York'
    time.tzset()
    logger.debug('FullfillmentCodeHook:bot_name={}, bot_alias={}, bot_version={}'\
                 .format(event['bot']['name'], event['bot']['alias'],\
                         event['bot']['version']))

    logger.debug('server url:{}, connection:{}'.format(server_url,server!=None))
    return dispatch(event)



