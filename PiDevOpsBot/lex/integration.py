from __future__ import print_function
import boto3
import os
import time
import datetime
import logging
import jenkins
import json
import xml.etree.ElementTree as ET

# --- global variables ---
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
server_url = 'http://jenkins.ilovethaitea.com:8080'

check_color_list = ['red','blue','aborted']
check_build_status_list = ['success','failure','aborted','successful','failed']
server_color_list = ['red','blue','aborted']
server_build_status_list = ['success','failure','aborted']


# --- create a global Jenkins instance
server = jenkins.Jenkins(server_url)
logger.debug("Connected to jenkins server")
version = server.get_version()
logger.debug("Jenkins version {}".format(version))
#target_dashboard_name = 'Integration_Dashboard'


# --- Helpers that build all of the responses ---

def confirm_intent(session_attributes, intent_name, slots,message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intent_name,
            'slots': slots,
            'message': message
        }
    }

def close(session_attributes, fulfillment_state, message):
    response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message
        }
    }

    return response

def delegate(session_attributes,slots):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Delegate',
            'slots': slots
        }
    }




# --- helper functions ---

def get_last_build_report(check_status):
    success_count = 0

    logger.debug('checking all {} jobs'.format(check_status))
    for job in job_name_list:
        #get job info by fullname
        job_info = server.get_job_info(job)
        if (job_info['lastBuild'] != None and job_info['lastSuccessfulBuild']) and \
           (job_info['lastBuild']['number'] == job_info['lastSuccessfulBuild']['number']):
            success_count += 1

    report=''
    if check_status == 'successful':
        report = '{} out of {} jobs are successful in last build.'\
                 .format(success_count,total_job_num)
    elif check_status == 'failed':
        report = '{} out of {} jobs failed in last build.'\
                 .format(total_job_num - success_count,total_job_num)
    
    return report

def get_color_report(check_color):
    color_count = 0
    logger.debug('checking number of {} jobs'.format(check_color))

    for job in job_name_list:
        job_info = server.get_job_info(job)
        if (job_info['color'] == check_color):
            color_count += 1

    return  '{} out of {} jobs are {}.'\
           .format(color_count,total_job_num,check_color)
    

def get_info_job(job_name, check_key):
    logger.debug('start getting info....')
    job_info = server.get_job_info(job_name)
    logger.debug('getting info of job {}'.format(job_name))
    result = ''
    if check_key == 'last successful build number':
        result = job_info['lastSuccessfulBuild']['number']
    elif check_key == 'last build number':
        result = job_info['lastBuild']['number']
    elif check_key == 'last successful build time':
        build_info = server.get_build_info(job_name,\
            job_info['lastSuccessfulBuild']['number'])
        result = tsTodt(build_info['timestamp'])
        result = str(result)
    elif check_key == 'url':
        result = job_info['url']
        s = 'url: <{}|{}>'.format(result,job_name)
        return s


    return 'The {} of {} is {}.'.format(check_key, job_name, result)

def tsTodt(timestamp):
    if timestamp == None:
        return '0'
    result = datetime.datetime.fromtimestamp(timestamp/1000)
    return result



def parse_build_code(build_code):
    if build_code == "last build":
        return 'lastBuild'
    elif build_code == 'last failed build':
        return 'lastFailedBuild'
    elif build_code == 'last unsuccessful build':
        return 'lastUnsuccessfulBuild'
    elif build_code == 'last successful build':
        return 'lastSuccessfulBuild'




def parse_build_query(build_query):
    if build_query == 'triggered' or build_query == 'executed' or build_query == 'executor':
        return 'causes'
    else:
        return 'claimed'

def get_build_info(job_name,build_query,build_code):
    binfo = server.get_build_info(job_name,build_code)
    b_actions = binfo['actions']

    logger.debug("build_code:{},build_info['actions']:{}".format(build_code,b_actions))

    if build_query == 'causes':
        for a in b_actions:
            if build_query in a:
                if 'userName' in a['causes'][0]:
                    executor = a['causes'][0]['userName']
                    userId = a['causes'][0]['userId']
                    return "Build:{} of {} is triggered by {}, userId:{}".format(build_code,job_name,executor,userId)
                elif 'upstreamProject' in a['causes'][0]:
                    return 'Cause: ' + a['causes'][0]['shortDescription']
        return "No execution record found."
    elif build_query == 'claimed':
        for a in b_actions:
            if build_query in a:
                if a['claimed']:
                    return "Build:{} of {} is claimed by {} on {}, reason: {}".format(build_code,job_name,a['claimedBy'],tsTodt(a['claimDate']), a['reason'])
               
        return "This build was not claimed."


def get_session_value(session_attributes,attribute):
    if attribute in session_attributes:
        return session_attributes[attribute]
    return None

def get_executor(b_actions):
    for a in b_actions:
        if 'causes' in a:
            if 'userName' in a['causes'][0]:
                return 'Executor: {}, userId: {}'.format(a['causes'][0]['userName'], a['causes'][0]['userId'])
            elif 'upstreamProject' in a['causes'][0]:
                return 'Cause: '+a['causes'][0]['shortDescription']
    return 'No execution record found.'


def get_claimer(b_actions):
    for a in b_actions:
        if 'claimed' in a:
            return 'Claimed by: {} on {}, reason: {}'.format(a['claimedBy'], tsTodt(a['claimDate']), a['reason'])
    return 'This build was not claimed.'


def get_job_list(view_name):
    view_config = server.get_view_config(view_name)
    tree = ET.ElementTree(ET.fromstring(view_config))
    root = tree.getroot()
    result = []
    if view_name == root.find('name').text:
        for job in root.iter('string'):
            result.append(job.text)
    return result

def get_status(job_name):
    stat = {}
    job_info = server.get_job_info(job_name)
    n_last_build = job_info['lastBuild']['number']
    build_info = server.get_build_info(job_name,n_last_build)
    stat['n_last_build'] = n_last_build
    stat['result'] = build_info['result']
    stat['executor'] = get_executor(build_info['actions'])
    stat['claimer'] = get_claimer(build_info['actions'])
    stat['url'] = job_info['url']
    return stat



def get_status_list(view_name,dash_query,dash_status):
    job_list = get_job_list(view_name)
    detail_list =''
    for job in job_list:
        stat = get_status(job)
        result = stat['result']
        if dash_status == 'all':
            detail_list += str(job_list.index(job)+1)+". "
            detail_list += '{}: \n\t last build number: {}\n\t result: {}\n\t {}\n\t {}\n\t url: <{}|{}>\n  '.format(job,stat['n_last_build'],\
                            stat['result'],stat['executor'],stat['claimer'],stat['url'],job)
        elif dash_status == 'successful' and result != None:
            if result.lower() == 'success':
                detail_list += str(job_list.index(job)+1)+". "
                detail_list += '{}: \n\t last build number: {}\n\t result: {}\n\t {}\n\t {}\n\t url: <{}|{}>\n  '.format(job,stat['n_last_build'],\
                            stat['result'],stat['executor'],stat['claimer'],stat['url'],job)
        elif dash_status == 'failed' and result != None:
            if result.lower() == 'failure':
                detail_list += str(job_list.index(job)+1)+". "
                detail_list += '{}: \n\t last build number: {}\n\t result: {}\n\t {}\n\t {}\n\t url: <{}|{}>\n  '.format(job,stat['n_last_build'],\
                            stat['result'],stat['executor'],stat['claimer'],stat['url'],job)
    if len(detail_list) == 0:
        detail_list  = 'No such job found'
        
    return detail_list


def get_env_report():
    dashboard_name = "Integration Dashboard"
    job_list = get_job_list()



# --- functions to handle intent requests ---
def query_all_status(intent_request):

    slots = intent_request['currentIntent']['slots']
    #status either successful or failed
    check_key = slots['CheckKey']

    logger.debug('checking all {} jobs'.format(check_key))
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    if check_key == None:
        check_key = get_session_value(session_attributes,'CheckKey')

    

    result = ''

    new_slot = intent_request['currentIntent']['slots']
   
    if check_key in check_color_list:
        result = get_color_report(check_key)

    elif check_key in check_build_status_list:
        result = get_last_build_report(check_key)
    else:
        new_slot['CheckKey'] = None

        return elicit_slot(session_attributes,
                            intent_request['currentIntent']['name'],
                            new_slot,
                            'CheckKey',
                            {'contentType': 'PlainText', 'content':'Which status do you want to check?'}
                            )
    session_attributes['query_all_status_result'] = result
    session_attributes['currentQuery'] = 'QueryAllStatus'
    session_attributes['CheckKey'] = check_key


    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType':'PlainText',
            'content':result
        }
    )

    

   
    
def query_job(intent_request):
    slots = intent_request['currentIntent']['slots']
    job_name = slots['JobName']
    check_key = slots['CheckKey']
    logger.debug('checking {} of job {}, passing to get_info_job'.format(check_key,job_name))
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    #uncomment later 
    if check_key == None:
        check_key = get_session_value(session_attributes,'CheckKey')
    if job_name == None:
        job_name = get_session_value(session_attributes,'JobName')

    
   
    
    result = get_info_job(job_name,check_key)
    session_attributes['currentQuery'] = 'QueryJob'
    session_attributes['CheckKey'] = check_key
    session_attributes['JobName'] = job_name
    session_attributes['query_job_result'] = result
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType':'PlainText',
            'content':result
        }
    )
    

def query_build(intent_request):
    slots = intent_request['currentIntent']['slots']
    job_name = slots['JobName']
    build_code  = slots['BuildCode']
    build_query = parse_build_query(slots['BuildQueryKey']) 
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    #uncomment later
    if job_name == None:
        job_name = get_session_value(session_attributes,'JobName')
    if build_code == None:
        build_code = get_session_value(session_attributes,'BuildCode')
    if build_query == None:
        build_query = get_session_value(session_attributes,'BuildQuery')



    if build_code.isdigit():
        build_code = int(build_code)

    else:
        jinfo = server.get_job_info(job_name)
        build_code = int(jinfo[parse_build_code(build_code)]['number'])

    logger.debug("Checking job:{},build num:{},build query:{}".format(job_name,build_code,build_query))

    result = get_build_info(job_name,build_query,build_code)

    session_attributes['JobName'] = job_name
    session_attributes['BuildQueryKey'] = build_query
    session_attributes['BuildCode'] = build_code
    session_attributes['query_build_result'] = result

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType':'PlainText',
            'content':result
        }
    )


def query_dashboard(intent_request):
    slots = intent_request['currentIntent']['slots']
    dashboard_name = slots['DashboardName']
    dash_query = slots['DashQueryKey']
    dash_status = slots['DashStatusKey']
    
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    result = ''
    if dash_query == 'list':
        result = get_status_list(dashboard_name,dash_query,dash_status)
        job_list = get_job_list(dashboard_name)
        for i in range(len(job_list)):
            session_attributes[str(i+1)] = job_list[i]

    session_attributes['DashboardName'] = dashboard_name



    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType':'PlainText',
            'content':result
        }
    )

def ask_env(intent_request):
    session_attributes = intent_request.get('sessionAttributes',{})
    result = get_env_report()
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType':'PlainText',
            'content':result
        }
    )



# --- direct request based on intent ---
def dispatch(intent_request):

    logger.debug('dispatch userId={}, intentName={}'\
                 .format(intent_request['userId'], \
                         intent_request['currentIntent']['name']))
    intent_name = intent_request['currentIntent']['name']

    if intent_name == 'QueryAllStatus':
        return query_all_status(intent_request)
    elif intent_name == 'QueryJob':
        return query_job(intent_request)
    elif intent_name == 'QueryBuild':
        return query_build(intent_request)
    elif intent_name == 'QueryDashBoard':
        return query_dashboard(intent_request)
    elif intent_name == 'AskEnv':
        return ask_env(intent_request)

    raise Exception('Intent with name ' + intent_name + ' not supported')
    

# --- Main handler ---
def handler(event, context):
    
    os.environ['TZ'] = 'America/New_York'
    time.tzset()
    logger.debug('FullfillmentCodeHook:bot_name={}, bot_alias={}, bot_version={}'\
                 .format(event['bot']['name'], event['bot']['alias'],\
                         event['bot']['version']))

    logger.debug('server url:{}, connection:{}'.format(server_url,server!=None))
    logger.debug('context: {}'.format(context))
    logger.debug('event: {}'.format(event))
    return dispatch(event)


